/*
* Author : Ashton Junior Ceaser Bradley
* Description: Array pointer utility with search
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#define ROW 50
#define COL 50
// Builds and returns pointer array of chars using ROW and COL 
char ** buildCharPointerArray();
//Adds contents to list 
void addToList(char name[COL] , char** list );
// prints  array
void printArray(char** list);
// Linear searches array 
bool searchArray(char name[COL] ,char** list);
char * convertTolower(char * n); 


char ** buildCharPointerArray(){
    // Allocate the block of memory for the entre array of character sets (String)
    char **nameList = (char **) malloc(ROW * COL * sizeof(char**));
     for(int x = 0 ; x < COL; x++){
        /* My compiler does not automattically null to the pointer 
        I am not using linux subsystem for windows, this is a win32 compiler issue
        */
      *(nameList+x) = NULL;
    }
    return nameList;
}

void addToList(char name[COL] , char** list ){
    for(int x = 0 ; x < ROW; x++){
        //searches for the next null box and stores the data inside of the array
        if(*(list+x) == NULL){
            //Access array by usiing pointer notation, shifting the address
            *(list+x) = (char *) malloc(COL * sizeof(char *));
            // Copies the strings value to the list pointer memory location
            strcpy(*(list+x),name);
            printf("Added %s \n" , *(list+x));
            // Once the value is added it exits the loop
            break;
        }
    }
}

// prints  array
void printArray(char** list){
    for(int x = 0 ; x < ROW; x++){
        if(*(list+x) != NULL){
            printf("%s\n" , *(list+x));
        }
    }
}

// Linear searches array 
bool searchArray(char name[COL] ,char** list){
    for(int x = 0 ; x < ROW; x++){
        if(strcmp(*(list+x),name) == 0 ){
             printf("Found %s\n", name);
            return true;
        }
    }
    printf("Didnt find %s\n", name);
    return false;
}

// Converts a string to lower
char * convertTolower(char * n){
    char *temp = (char*) malloc(sizeof(n));
    for(int x = 0 ; x < strlen(n);x++){
        *(temp+x) = tolower(*(n+x));
    }
    return temp;
}

// Converts a string to upper
char * convertToUpper(char * n){
    char *temp = (char*) malloc(sizeof(n));
    for(int x = 0 ; x < strlen(n);x++){
        *(temp+x) = toupper(*(n+x));
    }
    return temp;
}

