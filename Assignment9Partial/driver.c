/*
- Create a data entry interface , that allows users to input information such as name , salary , age.


- Your interface must have a menu to allow the user to Exit 


- Add, Search , Delete and Update information.


- This information must be stored in an array of structs.  
- You must create at least 3 elements in the array
- Pointers must be used.
- Print the contents of the structures after you have created the array.
- Add, Search and Delete Functions must be prototyped and written.
- Print the contents of the structure in the array element after each change.
- 
- There should be a function that adds all the salaries and gives the user results.
- Before you exit the program, print the contents of the array.
*/


#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>
#include<ctype.h>
#define BUFFSIZE 50
#define CACHESIZE 100


typedef struct PersonPayload {
int entryId;
int age;
char name[BUFFSIZE];
long salary;
}PPayLoad;

char * convertToUpper(char *n);
void PrintRecord(PPayLoad cache);
void Print(PPayLoad *cache);
void PrintNonDeleted(PPayLoad *cache);
void Add(PPayLoad *cache, int entryid, char name[BUFFSIZE], int age, long sal);
void Search(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal);
void Delete(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal);
void Update(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal);
void EventHandler(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal, int entryId);
void runMenu(PPayLoad *cache);
PPayLoad * prepCache();



int main(int arc , char** argv ){
PPayLoad *cache = prepCache();
runMenu(cache);
exit(EXIT_SUCCESS);
}

PPayLoad * prepCache(){
 PPayLoad *cache = (PPayLoad*) malloc(CACHESIZE * sizeof(PPayLoad));
        for(int x =0 ;x <  CACHESIZE; x++){
            cache[x].entryId = 0;
        }
return cache;
}

void runMenu(PPayLoad *cache){
    char name[BUFFSIZE];
    int age;
    long sal;
    bool dataEntry = true;
    bool debug = false;
    int action = 0;
    int entryId =1;

 while(dataEntry){
        printf("Type -1 if you are done entering data to exit\n"); 
        printf("1: Add \n 2: search \n 3: Delete\n  4: Update \n -1: Exit\n");
        scanf("%i", &action);
        if(action == -1 ){
            dataEntry = false;
            break;
        }
         
        printf("Enter Name:\n");
        scanf("%s", &name);
        if(atoi(name) == -1 ){
            dataEntry = false;
            break;
        }

        printf("Salary:\n");
        scanf("%ld", &sal);
        if(sal == -1){
            dataEntry = false;
            break;
        }
        
        printf("Age:\n");
        scanf("%i", &age);
        if(age == -1 ){
            dataEntry = false;
        break;
        }

   if(dataEntry ){
    /*Data Processing*/
    EventHandler(cache, action, name, age,  sal,entryId);
    entryId++;
   }
 }
}


char * convertToUpper(char *n){
    char *temp = (char*) malloc(sizeof(n));
    for(int x = 0 ; x < strlen(n);x++){
        *(temp+x) = toupper(*(n+x));
    }
    return temp;
}


void PrintRecord(PPayLoad cache){
    printf("---------- Print Record  --------------\n");
       printf("Id: %i,Age: %i,Name: %s, Salary $%ld\n",cache.entryId, cache.age,cache.name,cache.salary);
       printf("---------- Print Record  --------------\n");
}

void Print(PPayLoad *cache){
    printf("---------- Print Record  --------------\n");
     for(int x = 0 ; x < CACHESIZE;x++){
         printf("Id: %i,Age: %i,Name: %s, Salary $%ld\n",cache[x].entryId, cache[x].age,cache[x].name,cache[x].salary);
    }
    printf("---------- Print Record  --------------\n");
}

/*Prints no deleted entries*/
void PrintNonDeleted(PPayLoad *cache){
    printf("---------- Print Records  --------------\n");
     for(int x = 0 ; x < CACHESIZE;x++){
         if(cache[x].entryId > 0 ){
         printf("Id: %i,Age: %i,Name: %s, Salary $%ld\n",cache[x].entryId, cache[x].age,cache[x].name,cache[x].salary);
         }
    }
    printf("---------- Print Records --------------\n");
}


void Add(PPayLoad *cache, int entryid, char name[BUFFSIZE], int age, long sal){
    for(int x = 0 ; x < CACHESIZE;x++){
        if(cache[x].entryId == 0 ){
            cache[x].age =age;
            cache[x].entryId = entryid;
            strcpy(cache[x].name,name);
            cache[x].salary = sal;
        break;
        }
    }
}
void Search(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal){
     for(int x = 0 ; x < CACHESIZE;x++){
        if( cache[x].age == age && strcmp(convertToUpper(cache[x].name),convertToUpper(name)) == 0 && cache[x].salary == sal){
          printf("---------- Search  --------------\n");
          PrintRecord(cache[x]);
          printf("---------- Search  --------------\n");
         break;
        }
    }
}
void Delete(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal){
     for(int x = 0 ; x < CACHESIZE;x++){
         
        if( cache[x].age == age && strcmp(convertToUpper(cache[x].name),convertToUpper(name)) == 0 && cache[x].salary == sal){
         cache[x].entryId =0;
         cache[x].age =0;
         strcpy(cache[x].name,"");
         cache[x].salary =0;
         PrintRecord(cache[x]);
         break;
        }
    
    }

}

void Update(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal){
     for(int x = 0 ; x < CACHESIZE;x++){
        
        if( cache[x].age == age && strcmp(convertToUpper(cache[x].name),convertToUpper(name)) == 0 && cache[x].salary == sal){
         cache[x].entryId =0;
         cache[x].age =0;
         strcpy(cache[x].name,"");
         cache[x].salary =0;
         PrintRecord(cache[x]);
         break;
        }
    
    }

}




void EventHandler(PPayLoad *cache, int enventId, char name[BUFFSIZE], int age, long sal, int entryId){
    switch(enventId) {
        case 1:
         Add(cache, entryId,name,age,sal);
        break;
        case 2:
         Search(cache,entryId,name,age,sal);
        break;
        case 3:
         Delete(cache,entryId,name,age, sal);
        break;
        
        case 4:
         Update(cache,entryId,name,age, sal);
        break;

        default:
        break;
    }

   
    PrintNonDeleted(cache);
   

}
